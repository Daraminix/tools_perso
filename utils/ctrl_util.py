import maya.cmds as cmds
import ctrl_lib

def parentVisualLink(sourcePoint, destinationPoint, name):
	
	visualLink = ctrl_lib.createVisualLink(name=name)

	startClusterName = name + '_start_cluster'
	endClusterName   = name + '_end_cluster'

	startCluster = cmds.cluster(weightNode = visualLink + '.cv[0]', n=startClusterName)
	endCluster   = cmds.cluster(weightNode = visualLink + '.cv[1]', n=endClusterName)

	cmds.matchTransform(startCluster, sourcePoint)
	cmds.parent(startCluster, sourcePoint)

	cmds.matchTransform(endCluster, sourcePoint)
	cmds.parent(endCluster, sourcePoint)