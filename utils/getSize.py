import maya.cmds as cmds
def get_b_box_size():
	selection = cmds.ls(sl=True)
	bBox = cmds.exactWorldBoundingBox(selection)

	print('X Size:')
	print(abs(bBox[0])+abs(bBox[3]))
	print('Y Size:')
	print(abs(bBox[1])+abs(bBox[4]))
	print('Z Size:')
	print(abs(bBox[2])+abs(bBox[5]))