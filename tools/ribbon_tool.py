import maya.cmds as cmds

def make_ribbon(firstEdgeSelection=[],secondEdgeSelection=[], ribbonId='01', ctrlRatio=5, ctrlQuantity=10, rebuildCurve=True):

	'''
	make_ribbon create a ribbon between two edges selections.

	@firstEdgeSelection -> first selection of continious edges.
	@secondEdgeSelection -> second selection of continious edges.
	@ribbonId -> id number of the ribbon created
	@ctrlRatio -> number of sub joints driven by one controler
	@ctrlQuantity -> number of controler (note: one more is created to control the end of the ribbon)

	'''
	

	# - merge both list in one.
	edgeLoops = []
	edgeLoops.append(firstEdgeSelection)
	edgeLoops.append(secondEdgeSelection)

	if not edgeLoops:
		cmds.warning('[ Ribbon_'+ VERSION +' ] [makeRibbon] At least one selection is empty.... skipping \n'),
		return


	# - start variables.
	baseName    = ('ribbon_' + str(ribbonId))
	spansNumber = (ctrlQuantity * ctrlRatio)

	# - create names.
	nurbsName  = (baseName + '_nurbs')
	folGrpName = (baseName + '_follicles_grp')
	
	# - check if a ribbon with this id exist.
	if cmds.objExists(nurbsName):
		cmds.warning('[ Ribbon_'+ VERSION +' ] [makeRibbon] A ribbon with this ID already exist \n'),
		return

	# - set Ud attribute variables names.
	attrUdPrefix = 'JcRibbon'

	nurbsAttrUd         = (attrUdPrefix + 'Nurbs')
	grpAttrUd           = (attrUdPrefix + 'Grp')
	folGprAttrUd        = (attrUdPrefix + 'FolGrp')
	ctrlGrpAttrUd       = (attrUdPrefix + 'CtrlGrp')
	ctrlOffsetGrpAttrUd = (attrUdPrefix + 'OffsetCtrlGrp')
	folAttrUd           = (attrUdPrefix + 'Fol')
	ctrlAttrUd          = (attrUdPrefix + 'Ctrl')
	sknJntAttrUd        = (attrUdPrefix + 'SknJnt')

	# - create a set for created items.
	mainRibbonSet= ('main_ribbon_set')
	
	# - Check if the parent set exist.
	if not cmds.objExists(mainRibbonSet):
		cmds.sets(n=mainRibbonSet)
	
	# - create child sets for the specific ribbon.
	childRibbonSet  = (baseName + '_set')
	ribbonSknJntSet = (baseName + '_skn_jnt_set')
	ribbonFolSet    = (baseName + '_fol_set')
	ribbonCtrlSet   = (baseName + '_ctrl_set')

	cmds.sets(n=childRibbonSet)
	cmds.sets(childRibbonSet, add=mainRibbonSet)

	subSets = [ribbonSknJntSet, ribbonFolSet,ribbonCtrlSet]

	for subSet in subSets:
		cmds.sets(n=subSet)
		cmds.sets(subSet, add=childRibbonSet)


	# - create nurbs plane from edge selection.
	crvs  = (edge_loops_to_crvs(edgeLoops=edgeLoops, spansNumber=spansNumber))
	nurbs = (crvs_to_nurbs_plane(crvs=crvs))
	nurbs = (cmds.rename(nurbs, nurbsName))

	# - add Ud attribute for nurbs.
	cmds.addAttr(nurbs, ln=nurbsAttrUd, at='bool')
	cmds.setAttr(nurbs+ '.' + nurbsAttrUd, keyable=True, lock=True)


	# - get U and V spans for the hairs creation.
	spansV = (cmds.getAttr(nurbs+'.spansUV.spansV'))
	spansU = (cmds.getAttr(nurbs+'.spansUV.spansU'))
	
	folNumber = (spansU + spansV)

	# - create the follicles.
	folList = (create_follicles(folNumber = folNumber, baseName=baseName))
	folList = (attach_follicles(nurbsSurface=nurbs, folList=folList))

	# - create the follicle main group.
	folGrp = (cmds.group(name=folGrpName, w=True, empty=True ))
	cmds.addAttr(folGrp, ln=folGprAttrUd, at='bool')
	cmds.setAttr(folGrp + '.' + folGprAttrUd, keyable=True, lock=True)

	sknJntList = (create_joint_on_follicle(folList=folList, baseName=baseName))

	# - group follicules.
	for fol in folList:
		
		cmds.parent(fol, folGrp)

		cmds.sets(fol, add=ribbonFolSet)

		# - add Ud attr.
		cmds.addAttr(fol, ln=folAttrUd, at='bool')
		cmds.setAttr(fol+ '.' + folAttrUd, keyable=True, lock=True)

	# - add skin joints to sknJntSet.
	for sknJnt in sknJntList:
		
		cmds.sets(sknJnt, add=ribbonSknJntSet)

		cmds.addAttr(sknJnt, ln=sknJntAttrUd, at='bool')
		cmds.setAttr(sknJnt+ '.' + sknJntAttrUd, keyable=True, lock=True)


	# - create main group for controllers.
	ctrlGrpName = (baseName +  '_ribbon_ctrl_grp')
	
	ctrlGrp = (cmds.group(n=ctrlGrpName,w=1, em=1))
	cmds.addAttr(ctrlGrp, ln=ctrlGrpAttrUd, at='bool')
	cmds.setAttr(ctrlGrp+ '.' + ctrlGrpAttrUd, keyable=True, lock=True)

	# - create a follicle list for the control creation.

	folCtrlList = []

	for i in range(ctrlQuantity + 1):

		#number of the matching follicle.
		folIdNbr = ctrlRatio * i + 1

		#add 0 if under 10.
		folCntName = (make_digit_sortable(digitIn=folIdNbr))
		
		folCtrlList.append(baseName + '_' + folCntName + '_follicle')

	
	offsetGrpList = (create_ribbon_ctrl(folCtrlList=folCtrlList, baseName=baseName))

	# - create ctrl group and what's inside.
	# - create offset group and parent it to a global control group.

	
	ctrlList=[]
	
	for offsetGrp in offsetGrpList:

		cmds.parent(offsetGrp, ctrlGrp)
		cmds.addAttr(offsetGrp, ln=ctrlOffsetGrpAttrUd, at='bool')
		cmds.setAttr(offsetGrp+ '.' + ctrlOffsetGrpAttrUd, keyable=True, lock=True)


		# - get the ctrl joint.
		ctrlJnt = (cmds.listRelatives(offsetGrp)[0])

		# - uD attribute.
		cmds.addAttr(ctrlJnt, ln=ctrlAttrUd, at='bool')
		cmds.setAttr(ctrlJnt+ '.' + ctrlAttrUd, keyable=True, lock=True)

		# - add ctrl to set.
		cmds.sets(ctrlJnt, add=ribbonCtrlSet)

		ctrlList.append(ctrlJnt)

	# - get the nurbs shape.
	nurbsSurfShape = (cmds.ls(nurbs, dag=True, ni=True, type='nurbsSurface')[0])

	# - skin the control joints on the nurbs shape.
	skinCluster = (cmds.skinCluster(ctrlList, nurbsSurfShape))



	# - parent everithing into main group.
	mainRibbonGrpName = (baseName + '_' + 'grp')

	# - create main grp.
	mainRibbonGrp = (cmds.group(n=mainRibbonGrpName, w=True, em=True))

	cmds.addAttr(mainRibbonGrp, ln=grpAttrUd, at='bool')
	cmds.setAttr(mainRibbonGrp+ '.' + grpAttrUd, keyable=True, lock=True)

	# - parent nurbs to main grp.
	cmds.parent(nurbs, mainRibbonGrp)

	# - parent follicle group into main group.
	cmds.parent(folGrp, mainRibbonGrp)

	# - parent control group into main group.
	cmds.parent(ctrlGrp, mainRibbonGrp)

	# import ribbon_v07 as ribbon
	# reload(ribbon)
	# curentFirstEdgeSel=cmds.ls(sl=True)
	# curentSecondEdgeSel=cmds.ls(sl=True)
	# curentId='01'
	# curentCtrlRatio=5
	# curentCtrlQuantity=10

	# ribbon.make_ribbon(firstEdgeSelection=curentFirstEdgeSel,secondEdgeSelection=curentSecondEdgeSel, ribbonId=curentId, ctrlRatio=curentCtrlRatio, ctrlQuantity=curentCtrlQuantity)



def create_follicles(folNumber, baseName):

	'''
	create_follicles create follicles nodes
	@folNumber -> number of follicles created. a follicle is created for each span, plus one for the end
	@baseName - > the prefix for the new follicle created, must include the id

	@return a list of the newly created follicles stored in the folList variable
	
	'''
	# *setAttr "ribbon_01_06_follicleShape.simulationMethod" 0;

	folList = []
	
	for folCnt in range(folNumber):

		folCnt += 1

		# - add 0 if under 10.
		folCntName = (make_digit_sortable(digitIn=folCnt))

		# - create follicle name
		folName = (baseName + '_' + str(folCntName) + '_follicle')

		folShape = (cmds.createNode("follicle"))
		folTransform = (cmds.listRelatives(folShape, p=True))
		fol = (cmds.rename(folTransform, folName))
		cmds.setAttr(fol + '.simulationMethod', 0)
	
		folList.append(fol)

	return(folList)

	# import ribbon_v07 as ribbon
	# reload(ribbon)
	# ribbon.create_follicles(folNumber=50, baseName='toto')



def attach_follicles(nurbsSurface, folList):

	'''
	
	attach_follicles attach follicles from a list on a nurbs surface.

	@nurbsSurface -> the name of the nurbs surface on wich the follicles are gonna get attachet (type = string).
	@folList      -> list containing the follicles to attach.

	@return the follicles list as it came in
	
	'''

	# - links the follicle transform to the shape.

	for fol in folList:
		
		folTransform = fol
		folShape     = (cmds.listRelatives(folTransform, c=True))[0]

		# - set plugs names.
		folTrsfTransPlug = (folTransform + '.translate')
		folTrsfRotPlug   = (folTransform + '.rotate')

		folShapeOutTransPlug = (folShape + '.outTranslate')
		folShapeOutRotPlug   = (folShape + '.outRotate')

		# - link plugs together.
		cmds.connectAttr(folShapeOutTransPlug, folTrsfTransPlug, force=True)
		cmds.connectAttr(folShapeOutRotPlug, folTrsfRotPlug, force=True)

		nurbsShapeLocalPlug       = (nurbsSurface + '.local')
		nurbsShapeWorldMatrixPlug = (nurbsSurface + '.worldMatrix[0]')

		folShapeInputSurfacePlug     = (folShape + '.inputSurface')
		folShapeInputWorldMatrixPlug = (folShape + '.inputWorldMatrix')

		cmds.connectAttr(nurbsShapeLocalPlug, folShapeInputSurfacePlug, force=True)
		cmds.connectAttr(nurbsShapeWorldMatrixPlug, folShapeInputWorldMatrixPlug, force=True)


	# - get the number of follicles to attach
	folNbr = (len(folList))

	# - set the follicle Uv parameters depending on the longest parameter.
	spansU = float(cmds.getAttr(nurbsSurface+'.spansUV.spansU'))
	spansV = float(cmds.getAttr(nurbsSurface+'.spansUV.spansV'))



	if spansU > spansV:

		folVPos  = (spansV / 2)
		folUStep = (1  /spansU)
		
		for step in range (folNbr):
			
			folUPos = (step * folUStep)

			fol = folList[step]

			folParameterUPlug = (fol + '.parameterU')
			folParameterVPlug = (fol + '.parameterV')

			cmds.setAttr(folParameterUPlug, folUPos)
			cmds.setAttr(folParameterVPlug, folVPos)

	else: 
		
		folUPos  = (spansU / 2)
		folVStep = (1 / spansV)
			
		for step in range (folNbr):
			
			folVPos = (step * folVStep)

			fol = folList[step]

			folParameterUPlug = (fol + '.parameterU')
			folParameterVPlug = (fol + '.parameterV')

			cmds.setAttr(folParameterUPlug, folUPos)
			cmds.setAttr(folParameterVPlug, folVPos)

	return(folList)


	# import ribbon_v07 as ribbon
	# reload(ribbon)
	# curentFolList = cmds.ls(sl=True)
	# ribbon.create_follicles(nurbsSurface = 'your nurbs surface name', folList=curentFolList)


def edge_loops_to_crvs(edgeLoops, spansNumber, rebuildCurve=True):

	'''
	edge_loops_to_crvs create curves from edge list and rebuid them

	@edgeLoops -> list of edge loops list
	@spansNumber -> spans number of the rebuilded curve

	@return -> list of newly build curves

	''' 
	crvs = []
	
	for edgeLoop in edgeLoops:

		# - create curve from edge loop.
		cmds.select(edgeLoop)
		crv = (cmds.polyToCurve(ch=False))

		if rebuildCurve:
			# - rebuild curve.
			crv = (cmds.rebuildCurve(crv, constructionHistory=False, keepEndPoints=False , spans=spansNumber))
		
		crvs.append(crv)

	return(crvs)

def crvs_to_nurbs_plane(crvs):

	'''

	loft two curves together, output a nurbs plane and delete the input curves

	@crvs -> list of the curves to loft

	@return -> name of the nurbs created

	'''
	# - loft the curves.
	nurbs = (cmds.loft(crvs[0], crvs[1])[0])

	# - delete curves.
	cmds.delete(crvs[0], crvs[1])

	return(nurbs)


def create_joint_on_follicle(folList, baseName):

	'''

	create a joint parented to a follicle

	@folList -> list of the follicles needing a joint
	@baseName -> prefix for the joint name

	@return -> list of the joint created

	'''

	sknJntList = []
	folCnt     = 1
	
	for fol in folList:

		# - add 0 if uner 10.
		folCntName = (make_digit_sortable(digitIn=folCnt))

		# - setup names used in loop.
		sknJntName= (baseName +'_' + str(folCntName) +'_' + 'ribbon_skn_jnt')
		
		# - create joints.
		sknJnt = (cmds.joint(a=True, n=sknJntName))
		cmds.parent(sknJnt, fol)
		cmds.setAttr(sknJnt+'.tx',0)
		cmds.setAttr(sknJnt+'.ty',0)
		cmds.setAttr(sknJnt+'.tz',0)

		sknJntList.append(sknJnt)

		# - increment counter.
		folCnt += 1

	return(sknJntList)

def create_ribbon_ctrl(folCtrlList, baseName):

	'''

	create_ribbon_ctrl create a joint and a control shape 

	@folCtrlList -> list of the follicles where a controler is going to be created
	@baseName -> prefix for the joint name

	@return -> list of the offset groups created

	'''

	ctrlCnt       = 1
	offsetGrpList = []

	for fol in folCtrlList:

		# - add 0 if under 10.
		ctrlCntName = (make_digit_sortable(digitIn=ctrlCnt))

		# - create nave variable for the loop.
		offsetGrpName = (baseName + '_' + ctrlCntName + '_ribbon_ctrl_offset_grp')
		ctrlName      = (baseName + '_' + ctrlCntName + '_ribbon_ctrl')
		ctrlJointName = (baseName + '_' + ctrlCntName + '_ribbon_ctrl_jnt')

		# - create ctrl group and what's inside.
		# - create offset group and parent it to a global control group.
		offsetGrp = (cmds.group(n=offsetGrpName,w=1, em=1))
		offsetGrpList.append(offsetGrp)


		# - create control shape and parent it to his folder.
		ctrl = (cmds.circle(n=ctrlName,ch=0,r=0.5, nr=[0,1,0]))
		cmds.parent(ctrl, offsetGrp)

		# - create joint to control the nurbs. parented to the control shape directly.
		cmds.select(offsetGrp)
		ctrlJnt = (cmds.joint(name=ctrlJointName,rad=1.5))

		# - parent ctrl shape to joint.
		cmds.parent(ctrl[0]+'Shape', ctrlJnt, s=True, r=True)
		cmds.delete(ctrl)	
		ctrlJnt = (cmds.rename(ctrlJnt, ctrlName))

		# - match transformation from the child joint.
		cmds.matchTransform(offsetGrp, fol, pos=1,rot=1)

		# -  edit variable count value for the loop.
		ctrlCnt += 1
	
	return(offsetGrpList)


def make_digit_sortable(digitIn, size=2):

	'''

	add a 0 before the digit if it's under 10

	@digitIn -> the digit you want to make sorting_compatible

	@return -> the digit with the correct nomenclature 

	'''
	while len(str(digitIn)) < size:
		
		digitIn = ('0'+ str(digitIn))


	digitOut = (str(digitIn))

	return(digitOut)
