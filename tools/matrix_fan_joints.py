import maya.cmds as cmds

import syntax_lib


def buildFanJoints(inputJnts=[], rotAxis='X'):

	syn = syntax_lib.syntax()

	ftFanJointsList = []
	bkFanJointsList = []

	for jnt in inputJnts:


		upJnt  = cmds.listRelatives(jnt, allParents=True, type='joint')[0]
		midJnt = jnt
		dnJnt  = cmds.listRelatives(jnt, allDescendents=True, type='joint')[0]

		ftFanJointName =jnt.replace('_' + syn.skn + '_' + syn.jnt, '_' + syn.ft + '_' + syn.fan + '_' + syn.jnt)
		ftFanJoint = fanJoints(upJnt, midJnt, dnJnt, rotAxis='X', side=syn.ft, fanJointName=ftFanJointName)
		ftFanJointsList.append(ftFanJoint)

		
		bkFanJointName =jnt.replace('_' + syn.skn + '_' + syn.jnt, '_' + syn.bk + '_' + syn.fan + '_' + syn.jnt)
		bkFanJoint = fanJoints(upJnt, midJnt, dnJnt, rotAxis='X', side=syn.bk, fanJointName=bkFanJointName)
		bkFanJointsList.append(bkFanJoint)





def fanJoints(upJnt, midJnt, dnJnt, rotAxis, side, fanJointName):

	syn = syntax_lib.syntax()

	# - Define nodes names

	upJtDmName       = fanJointName + '_' + syn.up    + '_' + syn.DM
	midJtDmName      = fanJointName + '_' + syn.mid   + '_' + syn.DM
	dnJtDmName       = fanJointName + '_' + syn.dn    + '_' + syn.DM

	upVectorPmaName  = fanJointName + '_' + syn.up    + '_' + syn.vector + '_' + syn.PMA
	dnVectorPmaName  = fanJointName + '_' + syn.dn    + '_' + syn.vector + '_' + syn.PMA
		
	angleBetweenName = fanJointName + '_' + syn.angle + '_' + syn.AB
	angleRatioMdName = fanJointName + '_' + syn.angle + '_' + syn.ratio + '_' + syn.MD
		
	fanScaleXRvName  = fanJointName + '_' + syn.scale + '_' + 'X' + '_' + syn.RV
	fanScaleYRvName  = fanJointName + '_' + syn.scale + '_' + 'Y' + '_' + syn.RV
	fanScaleZRvName  = fanJointName + '_' + syn.scale + '_' + 'Z' + '_' + syn.RV

	# - Create nodes


	upJtDm       = cmds.createNode('decomposeMatrix', name=upJtDmName)
	midJtDm      = cmds.createNode('decomposeMatrix', name=midJtDmName)
	dnJtDm       = cmds.createNode('decomposeMatrix', name=dnJtDmName)
		
	upVectorPma  = cmds.createNode('plusMinusAverage', name=upVectorPmaName)
	dnVectorPma  = cmds.createNode('plusMinusAverage', name=dnVectorPmaName)
		
	angleBetween = cmds.createNode('angleBetween',     name=angleBetweenName)	
	angleRatioMd = cmds.createNode('multiplyDivide',   name=angleRatioMdName)	
		
	fanScaleXRv  = cmds.createNode('remapValue',       name=fanScaleXRvName)
	fanScaleYRv  = cmds.createNode('remapValue',       name=fanScaleYRvName)
	fanScaleZRv  = cmds.createNode('remapValue',       name=fanScaleZRvName)

	# - Create fan joint

	fanJoint = cmds.duplicate(midJnt, name=fanJointName, parentOnly=True)[0]

	cmds.parent(fanJoint, midJnt)

	# - Create custom attributes

	fanAttrMenuName = side +'_' + syn.fan + '_' + syn.jnt
	fanMaxRotAttr   = syn.max +'_' + syn.rot
	fanScaleXAttr   = syn.scale +'_' + 'x' + '_' + side
	fanScaleYAttr   = syn.scale +'_' + 'y' + '_' + side
	fanScaleZAttr   = syn.scale +'_' + 'z' + '_' + side

	cmds.addAttr(fanJoint, longName=fanAttrMenuName, attributeType="enum", enumName='-----', keyable=True)
	cmds.setAttr(fanJoint + '.' + fanAttrMenuName, keyable=True, lock=True)
	cmds.addAttr(fanJoint, longName=fanMaxRotAttr, attributeType="float", min=-360, max=360, defaultValue=0, keyable=True)
	cmds.addAttr(fanJoint, longName=fanScaleXAttr, attributeType="float", min=1, max=2, defaultValue=1, keyable=True)
	cmds.addAttr(fanJoint, longName=fanScaleYAttr, attributeType="float", min=1, max=2, defaultValue=1, keyable=True)
	cmds.addAttr(fanJoint, longName=fanScaleZAttr, attributeType="float", min=1, max=2, defaultValue=1, keyable=True)

	# - Set plug names
		
	fanMaxRotPlug = fanJoint + '.' + fanMaxRotAttr
	fanScaleXPlug = fanJoint + '.' + fanScaleXAttr
	fanScaleYPlug = fanJoint + '.' + fanScaleYAttr
	fanScaleZPlug = fanJoint + '.' + fanScaleZAttr

	upJntWorldMatrixOutPlug  = upJnt  + '.worldMatrix'
	midJntWorldMatrixOutPlug = midJnt + '.worldMatrix'
	dnJntWorldMatrixOutPlug  = dnJnt  + '.worldMatrix'

	upJtDmMatrixInPlug  = upJtDm  + '.inputMatrix'
	midJtDmMatrixInPlug = midJtDm + '.inputMatrix'
	dnJtDmMatrixInPlug  = dnJtDm  + '.inputMatrix'

	upJtDmMatrixOutPlug   = upJtDm   + '.outputTranslate'
	midJtDmMatrixOutPlug  = midJtDm  + '.outputTranslate'
	dnJtDmMatrixOutPlug   = dnJtDm   + '.outputTranslate'
		
	upVectorPmaIn3D0Plug     = upVectorPma + '.input3D[0]'
	upVectorPmaIn3D1Plug     = upVectorPma + '.input3D[1]'
	upVectorPmaOperationPlug = upVectorPma + '.operation'
		
	dnVectorPmaIn3D0Plug     = dnVectorPma + '.input3D[0]'
	dnVectorPmaIn3D1Plug     = dnVectorPma + '.input3D[1]'
	dnVectorPmaOperationPlug = dnVectorPma + '.operation'

	upVectorPmaOut3dPlug = upVectorPma + '.output3D'
	dnVectorPmaOut3dPlug = dnVectorPma + '.output3D'

	angleBetweenInVector1Plug = angleBetween + '.vector1'
	angleBetweenInVector2Plug = angleBetween + '.vector2'
	angleBetweenOutEulerPlug  = angleBetween + '.axisAngle.angle'

	angleRatioInput1XPlug = angleRatioMd + '.input1.input1X'
	angleRatioInput2XPlug = angleRatioMd + '.input2.input2X'
	angleRatioOutputXPlug = angleRatioMd + '.output.outputX'

	fanScaleXRvInputMaxPlug   = fanScaleXRv + '.inputMax'
	fanScaleXRvInputValuePlug = fanScaleXRv + '.inputValue'
	fanScaleXRvOutputMinPlug  = fanScaleXRv + '.outputMin'
	fanScaleXRvOutputMaxPlug  = fanScaleXRv + '.outputMax'
	fanScaleXRvOutValuePlug   = fanScaleXRv + '.outValue'

	fanScaleYRvInputMaxPlug   = fanScaleYRv + '.inputMax'
	fanScaleYRvInputValuePlug = fanScaleYRv + '.inputValue'
	fanScaleYRvOutputMinPlug  = fanScaleYRv + '.outputMin'
	fanScaleYRvOutputMaxPlug  = fanScaleYRv + '.outputMax'
	fanScaleYRvOutValuePlug   = fanScaleYRv + '.outValue'

	fanScaleZRvInputMaxPlug   = fanScaleZRv + '.inputMax'
	fanScaleZRvInputValuePlug = fanScaleZRv + '.inputValue'
	fanScaleZRvOutputMinPlug  = fanScaleZRv + '.outputMin'
	fanScaleZRvOutputMaxPlug  = fanScaleZRv + '.outputMax'
	fanScaleZRvOutValuePlug   = fanScaleZRv + '.outValue'

	fanJointRotateInPlug  = fanJoint + '.rotate.rotate' + rotAxis
	midJointRotateOutPlug = midJnt   + '.rotate.rotate' + rotAxis

	fanJointScaleXInPlug = fanJoint + '.scale.scaleX'
	fanJointScaleYInPlug = fanJoint + '.scale.scaleY'
	fanJointScaleZInPlug = fanJoint + '.scale.scaleZ'

	# - Connect plugs

	cmds.connectAttr(upJntWorldMatrixOutPlug,  upJtDmMatrixInPlug,  force=True)
	cmds.connectAttr(midJntWorldMatrixOutPlug, midJtDmMatrixInPlug, force=True)
	cmds.connectAttr(dnJntWorldMatrixOutPlug,  dnJtDmMatrixInPlug,  force=True)
		
	cmds.connectAttr(upJtDmMatrixOutPlug,  upVectorPmaIn3D0Plug, force=True)
	cmds.connectAttr(midJtDmMatrixOutPlug, upVectorPmaIn3D1Plug, force=True)
	cmds.connectAttr(midJtDmMatrixOutPlug, dnVectorPmaIn3D0Plug, force=True)
	cmds.connectAttr(dnJtDmMatrixOutPlug,  dnVectorPmaIn3D1Plug, force=True)
		
	cmds.connectAttr(upVectorPmaOut3dPlug, angleBetweenInVector1Plug, force=True)
	cmds.connectAttr(dnVectorPmaOut3dPlug, angleBetweenInVector2Plug, force=True)
		
	cmds.connectAttr(angleBetweenOutEulerPlug, angleRatioInput1XPlug, force=True)
		
	cmds.connectAttr(midJointRotateOutPlug, fanScaleXRvInputValuePlug, force=True)
	cmds.connectAttr(midJointRotateOutPlug, fanScaleYRvInputValuePlug, force=True)
	cmds.connectAttr(midJointRotateOutPlug, fanScaleZRvInputValuePlug, force=True)
	cmds.connectAttr(angleRatioOutputXPlug, fanJointRotateInPlug,      force=True)
		
	cmds.connectAttr(fanMaxRotPlug, fanScaleXRvInputMaxPlug, force=True)
	cmds.connectAttr(fanMaxRotPlug, fanScaleYRvInputMaxPlug, force=True)
	cmds.connectAttr(fanMaxRotPlug, fanScaleZRvInputMaxPlug, force=True)

	cmds.connectAttr(fanScaleXPlug, fanScaleXRvOutputMaxPlug, force=True)
	cmds.connectAttr(fanScaleYPlug, fanScaleYRvOutputMaxPlug, force=True)
	cmds.connectAttr(fanScaleZPlug, fanScaleZRvOutputMaxPlug, force=True)

	cmds.connectAttr(fanScaleXRvOutValuePlug, fanJointScaleXInPlug, force=True)
	cmds.connectAttr(fanScaleYRvOutValuePlug, fanJointScaleYInPlug, force=True)
	cmds.connectAttr(fanScaleZRvOutValuePlug, fanJointScaleZInPlug, force=True)

	cmds.setAttr(fanScaleXRvOutputMinPlug, 1)
	cmds.setAttr(fanScaleYRvOutputMinPlug, 1)
	cmds.setAttr(fanScaleZRvOutputMinPlug, 1)

	cmds.setAttr(angleRatioInput2XPlug, -0.5)
	cmds.setAttr(dnVectorPmaOperationPlug, 2)
	cmds.setAttr(upVectorPmaOperationPlug, 2)

	jntLabeling(jnt=fanJoint, drawLabel=False)
	
	return(fanJoint)	


def jntLabeling(jnt, drawLabel=True):

	syn = syntax_lib.syntax()

	labelName = jnt[2:]

	if jnt[0] == syn.l:
		cmds.setAttr(jnt + '.side', 1)
	
	elif jnt[0] == syn.r:
		cmds.setAttr(jnt + '.side', 2)

	else:
		cmds.setAttr(jnt + '.side', 0)


	cmds.setAttr(jnt + '.type', 18)


	cmds.setAttr(jnt + '.otherType', labelName, type='string')

	if drawLabel == True:
		cmds.setAttr(jnt + '.drawLabel', 1)