import ribbon_tool
import maya.cmds as cmds


def mdHandSnap(jnt_sel=[]):
    for jnt in jnt_sel:
        ctrlSnap = jnt.replace('_rev_jnt','_ctrl_snap_grp')
        mdName = jnt.replace('_rev_jnt','_snap_MD')


        multiplyDivide = cmds.createNode('multiplyDivide', name=mdName)

        jntOutRotationPlug = jnt            + '.rotate'
        mdInput1Plug       = multiplyDivide + '.input1'
        mdOutputPlug       = multiplyDivide + '.output'
        ctrlSnapInRotPlug  = ctrlSnap       + '.rotate'
        mdInput2XAttr      = multiplyDivide + '.input2.input2X'
        mdInput2YAttr      = multiplyDivide + '.input2.input2Y'
        mdInput2ZAttr      = multiplyDivide + '.input2.input2Z'

        cmds.connectAttr(jntOutRotationPlug,mdInput1Plug)
        cmds.connectAttr(mdOutputPlug,ctrlSnapInRotPlug)
        cmds.setAttr(mdInput2XAttr, -1)
        cmds.setAttr(mdInput2YAttr, -1)
        cmds.setAttr(mdInput2ZAttr, -1)

        cmds.setAttr(ctrlSnap + '.rotateOrder', 4)