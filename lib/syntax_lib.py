class syntax():

	def __init__(self):

		# - Suffixes

		self.jnt = 'jnt'
		self.msh = 'msh'
		self.skn = 'skn'
		self.fan = 'fan'


		# - Prefixes

		self.l   = 'l'
		self.r   = 'r'
		self.up  = 'up'
		self.dn  = 'dn'
		self.mid = 'mid'
		self.ft  = 'ft'
		self.bk  = 'bk'
		
		# - Elements

		self.matrix = 'matrix'
		self.vector = 'vector'
		self.angle  = 'angle'
		self.flip   = 'flip'
		
		# - Nodes

		self.PMA = 'PMA'
		self.MD  = 'MD'
		self.AB  = 'AB'
		self.RV  = 'RV'
		self.DM  = 'DM' 

		# - Attr

		self.rot   = 'rotation'
		self.scale = 'scale'
		self.trans = 'translate'

		# - Adjectives

		self.ratio = 'ratio'
		self.min   = 'min'
		self.max   = 'max'

