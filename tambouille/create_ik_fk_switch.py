import maya.cmds as cmds
import ctrl_lib
reload(ctrl_lib)


def createIkChain(sknJntChain=[], legId=''):

	'''
	
	Create an ik chain with controls from skin joints.

	@sknJntChain -> a list of skn joint
	@legId       -> the id of the leg. Used to create names.

	Return a list containing : -> the list of ik joints
							   -> the Wrist controler name
							   -> the pole vector controler name
							   -> the ik handle name

	'''
	
	ikJntChain = []
	
	# - Duplicate skin joint chain.
	for sknJnt in sknJntChain:	

		ikJntName = (sknJnt.replace('_skn_jnt', '_ik_jnt'))

		ikJnt = cmds.duplicate(sknJnt, n=ikJntName, parentOnly=True)

		ikJntChain.append(ikJnt[0])

	# - Reparent ik chain.
	cmds.parent(ikJntChain[1], ikJntChain[0], w=0)
	cmds.parent(ikJntChain[2], ikJntChain[1])

	# - Create ik Handle.
	ikHandleName = legId + 'IkHandle'
	ikHandle = (cmds.ikHandle(n=ikHandleName, startJoint=ikJntChain[0], endEffector=ikJntChain[2]))

	# - Create Ik controleurs
	
	wristCtrlName          = ikJntChain[2].replace('_ik_jnt', '_ik_ctrl')
	wristCtrlOffsetGrpName = ikJntChain[2].replace('_ik_jnt', '_ik_ctrl_offset_grp')

	wristCtrl           = ctrl_lib.createBearPaw(name=wristCtrlName, scale=0.5)
	wristCtrlOffsetGrp  = cmds.group(n=wristCtrlOffsetGrpName, w=True, empty=True)
	cmds.parent(wristCtrl, wristCtrlOffsetGrp)
	cmds.matchTransform(wristCtrlOffsetGrp, sknJntChain[2])
	cmds.parent(ikHandle[0], wristCtrl)
	cmds.orientConstraint(wristCtrl,ikJntChain[2])

	# - Create pole vector

	# - Create pole vector control shape

	poleVectorCtrlName            = (legId + '_pole_vector_ctrl')
	poleVectorCtrlOffsetGroupName = (legId + '_pole_vector_ctrl_offset_grp')
	
	poleVectorCtrl                = ctrl_lib.createDiamond(name=poleVectorCtrlName, scale=0.2)
	poleVectorCtrlOffsetGroup     = cmds.group(n=poleVectorCtrlOffsetGroupName, empty=True, w=True)



	# - Parenting and pole vector constraint
	cmds.parent(poleVectorCtrl, poleVectorCtrlOffsetGroup)

	parentConstraintPv = (cmds.parentConstraint(sknJntChain[0], sknJntChain[2], poleVectorCtrlOffsetGroup, maintainOffset=False))
	cmds.delete(parentConstraintPv)

	aimConstraintPv = cmds.aimConstraint(sknJntChain[1], poleVectorCtrlOffsetGroup, aimVector=(0,0,1))
	cmds.delete(aimConstraintPv)

	cmds.poleVectorConstraint(poleVectorCtrl, ikHandle[0])

	toReturn = [ikJntChain, wristCtrl, poleVectorCtrl, ikHandle[0]]

	return toReturn


def createFkChain(sknJntChain=[], legId=''):

	'''
	
	Create an fk chain with controls from skin joints.

	@sknJntChain -> a list of skn joint
	@legId       -> the id of the leg. Used to create names.

	Return a list containing : -> the list of fk joints
							   -> the list of fk controlers
							   -> the list of fk locators
							   

	'''

	fkJntChain = []

	for sknJnt in sknJntChain:	

		fkJntName = (sknJnt.replace('_skn_jnt', '_fk_jnt'))

		fkJnt = cmds.duplicate(sknJnt, n=fkJntName, parentOnly=True)

		fkJntChain.append(fkJnt[0])

	# - Reparent ik chain.
	cmds.parent(fkJntChain[1], fkJntChain[0], w=0)
	cmds.parent(fkJntChain[2], fkJntChain[1])

	fkCtrlList    = []
	fkLocatorList = []
	# - Create FK controlers and locator
	for fkJnt in fkJntChain:
		
		fkCtrlName          = fkJnt.replace('_fk_jnt', '_fk_ctrl')
		fkCtrlOffsetGrpName = fkJnt.replace('_fk_jnt', '_fk_ctrl_offset_grp')

		fkCtrl              = (cmds.circle(n=fkCtrlName, nr=[0,1,0]))[0]
		fkCtrlOffsetGrp     = cmds.group(name=fkCtrlOffsetGrpName, em=True, w=True)


		fkLocatorName       = fkJnt.replace('_fk_jnt', '_fk_ctrl_loc')
		fkLocator           = cmds.spaceLocator(n=fkLocatorName)

		cmds.parent(fkCtrl, fkLocator)
		cmds.parent(fkLocator, fkCtrlOffsetGrp)
		cmds.matchTransform(fkCtrlOffsetGrp, fkJnt)

		fkParentConstraintName = (fkJnt.replace('_fk_jnt', '_fk_ctrl_constraint')) 
		fkParentConstraint = cmds.parentConstraint(fkCtrl, fkJnt, n=fkParentConstraintName)

		try:
			cmds.parent(fkCtrlOffsetGrp,fkCtrlPrev)
		except:
			pass

		fkCtrlPrev = fkCtrl
		fkCtrlList.append(fkCtrl)
		fkLocatorList.append(fkLocator[0])

	toReturn =[fkJntChain, fkCtrlList, fkLocatorList]
	return toReturn



def createIkFkSwitch(sknJntChain=[], legId='toto'):

	'''
	
	Create an ik and fk one way switch using and fk and ik chain creators fonctions.

	@sknJntChain -> a list of skn joint
	@legId       -> the id of the leg. Used to create names.

	Return nothing yet
							   
	'''

	# - Retrieve ik and fk compoanant for the switch construction.
	
	ikComponent    = createIkChain(sknJntChain, legId)
	
	ikJntChain	   = ikComponent[0]
	wristCtrl      = ikComponent[1]
	poleVectorCtrl = ikComponent[2]
	ikHandle       = ikComponent[3]

	fkComponent    = createFkChain(sknJntChain, legId)
	
	fkJntChain     = fkComponent[0]
	fkCtrlList     = fkComponent[1]
	fkLocatorList  = fkComponent[2]

	ikFkCtrlName = legId + '_ik_fk_ctrl'
	ikFkCtrlOffsetGroupName = legId + '_ik_fk_ctrl_offset_grp'

	ikFkCtrl = ctrl_lib.createIkFkCtrl(name=ikFkCtrlName, scale=0.5)
	ikFkCtrlOffsetGroup = cmds.group(n=ikFkCtrlOffsetGroupName, empty=True, w=True)

	cmds.parent(ikFkCtrl, ikFkCtrlOffsetGroup)

	switchAttrName = legId + "_ik_fk_switch"
	cmds.addAttr(ikFkCtrl, longName= switchAttrName, attributeType='bool', k=True)

	# - Connect nodes with the condition controler. 

	ikSwitchPlug = (ikFkCtrl + '.' + switchAttrName)
	cmds.setAttr(ikSwitchPlug, True)

	reverseNodeName = switchAttrName + '_REV'
	
	cmds.createNode('reverse', n=reverseNodeName)

	reverseNodeInXPlug  = reverseNodeName + '.input.inputX'
	reverseNodeOutXPlug = reverseNodeName + '.output.outputX'

	cmds.connectAttr(ikSwitchPlug, reverseNodeInXPlug)

	wristFkCtrlSnapName = legId + '_wrist_fk_ctrl_snap'
	wristFkCtrlSnap     = (cmds.pointConstraint(fkCtrlList[2], wristCtrl, n=wristFkCtrlSnapName, maintainOffset=False))[0]

	wristFkCtrlSnapWheightPlug = wristFkCtrlSnap + '.' + fkCtrlList[2] + 'W0' 
	
	# - Manual wheight
	
	snapAttrName = legId + '_fk_ctrl_snap'
	cmds.addAttr(ikFkCtrl, longName= snapAttrName, attributeType='bool', k=True)
	cmds.connectAttr(ikFkCtrl + '.' + snapAttrName, wristFkCtrlSnapWheightPlug)



	# Dynamic wheight(don't work)
	# reverseNodeInYPlug  = reverseNodeName + '.input.inputY'
	# reverseNodeOutYPlug = reverseNodeName + '.output.outputY'

	# locWristVisibilityPlug = fkLocatorList[2] + '.visibility'

	# cmds.connectAttr(locWristVisibilityPlug, wristFkCtrlSnapWheightPlug)
	# # cmds.connectAttr(reverseNodeOutYPlug, wristFkCtrlSnapWheightPlug)

	# toto_wrist_fk_ctrl_snap.toto_wrist_fk_ctrlW0

	for index in range(3):

		# - Create parent constraint to skn joints.

		parentConstraint = cmds.parentConstraint(ikJntChain[index], fkJntChain[index], sknJntChain[index])

		ikWeightPlug = parentConstraint[0] + '.' + ikJntChain[index] + 'W0'
		fkWeightPlug = parentConstraint[0] + '.' + fkJntChain[index] + 'W1'

		cmds.connectAttr(ikSwitchPlug, ikWeightPlug)
		cmds.connectAttr(reverseNodeOutXPlug, fkWeightPlug)

		conditionNodeName = ikJntChain[index] + '_loc_rot_C' 
		conditionNode = cmds.createNode('condition', n=conditionNodeName)
		cmds.setAttr(conditionNode + '.colorIfFalseR', 0)
		cmds.setAttr(conditionNode + '.colorIfFalseG', 0)
		cmds.setAttr(conditionNode + '.colorIfFalseB', 0)

		# - Define Plugs.
		
		ikJntRotatePlug        = ikJntChain[index]    + '.rotate'
		conditionInPlug        = conditionNode        + '.colorIfTrue'
		conditionOutPlug       = conditionNode        + '.outColor'
		conditionStatementPlug = conditionNode        + '.firstTerm'
		locatorRotatePlug      = fkLocatorList[index] + '.rotate'
		

		# - ConnectPlugs

		cmds.connectAttr(ikJntRotatePlug, conditionInPlug)
		cmds.connectAttr(conditionOutPlug, locatorRotatePlug)
		cmds.connectAttr(ikSwitchPlug, conditionStatementPlug)

	ikBlendPlug = ikHandle + '.ikBlend'
	cmds.connectAttr(ikSwitchPlug, ikBlendPlug)
	
	# - Dynamic Visibility

	for fkCtrl in fkCtrlList:
		visibilityPlug = fkCtrl + '.visibility'
		cmds.connectAttr(reverseNodeOutXPlug, visibilityPlug)
	
	for locator in fkLocatorList:
		visibilityPlug = locator + '.visibility'
		cmds.connectAttr(reverseNodeOutXPlug, visibilityPlug)
	
	for fkJnt in fkJntChain:
		visibilityPlug = fkJnt + '.visibility'
		cmds.connectAttr(reverseNodeOutXPlug, visibilityPlug)
	
	for ikJnt in ikJntChain:
		visibilityPlug = fkJnt + '.visibility'
		cmds.connectAttr(ikSwitchPlug, visibilityPlug)


	poleVectorVisibilityPlug = poleVectorCtrl + '.visibility'
	cmds.connectAttr(ikSwitchPlug, poleVectorVisibilityPlug)








# 	import sys
# sys.path.append('G:/ours_cs/assets')

# import  ik_fk
# reload(ik_fk)

# sknJntChain=(cmds.ls(sl=True))

# ik_fk.createIkChain(sknJntChain=sknJntChain)