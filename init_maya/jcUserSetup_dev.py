# - define modules.
import sys, os
import getpass


def addDirPathInMayaPython():
	'''
		- define os platform and build python maya script path.

		return dirPath.
	'''

	osPlatform = sys.platform

	dirPath = None

	mayaPythonPath = sys.path

	if osPlatform == 'win32':
		dirPath = ('D:/dev_perso')
	
	if not dirPath in mayaPythonPath:
		sys.path.append(dirPath)
	
	return dirPath


def initUserSetup():


	print('---')
	print(' [ ADD ] : ADD PYTHON DIR PATH ...\n'),
	addDirPathInMayaPython()


	print('---')
	print(' [ IMPORT ] : JC INIT ...\n'),
	exec('import jcInit') in globals()
	exec('reload(jcInit)') in globals()


	print('---')
	print(' [ IMPORT ] : JC SCRIPTS ...\n'),
	jcInit.importScripts()

	print('\n---')
	print(' [ END PROCESS. SET UP DEV READY ] ...\n\n'),