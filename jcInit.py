# - import modules.
import os, sys
import re

import maya.cmds as cmds

# -get maya version.
maya_version = int(cmds.about(v=True))

DEV_PATH = 'D:/dev_perso'

# - global vars.
dirsPaths  = [	'/lib', 
				'/tambouille', 
				'/tools',   
				'/utils', 
				]

exeptions = []

def addPathToSysPath(dirs):
		
	# - define path from kb hierarchy.
	jcDirPath = DEV_PATH
	dirsPath    = []
	
	# - collect custom paths.
	for dir in dirs:

		dirPath =(jcDirPath + dir)
		dirsPath.append(dirPath)
		
	# - append jcDirPath.
	dirsPath.append(jcDirPath)
	
	
	# - append path to sys.
	for dirPath in dirsPath:

		if(os.path.exists(dirPath)):
			
			paths = sys.path

			# - if exists. Skip.
			if dirPath in paths:
				print ' directory found  :>   {}\n'.format(dirPath),
					
			# - else, append to maya sys paths.	
			if not dirPath in paths:
				sys.path.append(dirPath)
				print ' directory append :>   {}\n'.format(dirPath),

	return dirsPath


def importScripts():
	
	global dirsPaths
	global exceptions
	
	
	# - fill pythonPaths var to import modules.
	dirsPath = addPathToSysPath(dirsPaths)


	# - remove scripts path
	jcDirPath = DEV_PATH
	dirsPath.remove(jcDirPath)
	
	# - find python files.
	for dirPath in dirsPath:
		
		# - check if all dirPaths exists.
		if(os.path.isdir(dirPath) == True):
			listDir = os.listdir(dirPath)

			for file in listDir:
				# - get only files. (need full path).
				if(os.path.isfile( dirPath +'/'+ file) == True):
				
					# - split .py file.
					filename   = file.split('.')[0]
					fileType   = file.split('.')[-1]

					if not filename:
						print ' directory unexpected  ++ :>   {}. Does not exist.\n'.format(dirPath),
						continue

					# - import modules.
					if not filename in exceptions and fileType == 'py':	
						exec('import ' + filename) in globals()
						exec('reload(' + filename +')') in globals()
						pyFiles.append(filename)

					# - source mel scripts.
					if not filename in exceptions and fileType == 'mel':
						mel.eval('source' +'"'+ dirPath +"/"+ file +'"')
						melFiles.append(file)
						
		else:
			print ' directory unexpected  :>   {}. Does not exist.\n'.format(dirPath),
			
			
	# # - print pyFiles found.	
	# for file in pyFiles:
	# 	if(file != '__init__'):
	# 		print ' module imported	 :>   {}\n'.format(file),

	# # - print melFiles found.	
	# for file in melFiles:
	# 		print ' scriptFile sourced	 :>   {}\n'.format(file),
