import maya.cmds as cmds

#create ft_fan joints

mid_jnts=(cmds.ls(sl=True))

for mid_jnt in mid_jnts:
    ft_fan_jnt= mid_jnt.replace("jnt","ft_fan_jnt")
    
    cmds.duplicate(mid_jnt,name=ft_fan_jnt, parentOnly=True)
    cmds.parent(ft_fan_jnt,mid_jnt.replace("jnt","snapper"))
    
    up_jnt=cmds.pickWalk(mid_jnt,d="up")

    dn_jnt=cmds.pickWalk(mid_jnt,d="down")

    #create_angle_calculation_names
    up_vector_PMA=ft_fan_jnt.replace("jnt","up_vector_PMA")
    dn_vector_PMA=ft_fan_jnt.replace("jnt","dn_vector_PMA")
    fan_angle_AB=ft_fan_jnt.replace("jnt","angle_AB")
    fan_angle_MD=ft_fan_jnt.replace("jnt","angle_direction_MD")
    fan_angle_X_PMA=ft_fan_jnt.replace("jnt","angle_X_PMA")
    fan_angle_Y_PMA=ft_fan_jnt.replace("jnt","angle_Y_PMA")
    fan_angle_Z_PMA=ft_fan_jnt.replace("jnt","angle_Z_PMA")
    
    
    #creates_vector_nodes    
    
    cmds.createNode("plusMinusAverage", name=up_vector_PMA)
    cmds.setAttr((up_vector_PMA+".operation"),2)
    
    cmds.createNode("plusMinusAverage", name=dn_vector_PMA)
    cmds.setAttr((dn_vector_PMA+".operation"),2)
    
    #connect vector nodes
    
    cmds.connectAttr(up_jnt[0].replace("jnt","snapper")+".translate", up_vector_PMA+".input3D[0]")
    cmds.connectAttr(mid_jnt.replace("jnt","snapper")+".translate", up_vector_PMA+".input3D[1]")
    cmds.connectAttr(mid_jnt.replace("jnt","snapper")+".translate", dn_vector_PMA+".input3D[0]")
    cmds.connectAttr(dn_jnt[0].replace("jnt","snapper")+".translate", dn_vector_PMA+".input3D[1]")
    
    #angle between
    
    cmds.createNode("angleBetween", name=fan_angle_AB)
    cmds.connectAttr(up_vector_PMA+".output3D", fan_angle_AB+".vector1")
    cmds.connectAttr(dn_vector_PMA+".output3D", fan_angle_AB+".vector2")
    
    #remap euler
    
    cmds.createNode("multiplyDivide", name=fan_angle_MD)
    cmds.connectAttr(fan_angle_AB+".eulerX", fan_angle_MD+".input1X")
    cmds.connectAttr(fan_angle_AB+".eulerY", fan_angle_MD+".input1Z")
    cmds.connectAttr(fan_angle_AB+".eulerZ", fan_angle_MD+".input1Y")
    cmds.setAttr(fan_angle_MD+".input2X",-0.5)
    cmds.setAttr(fan_angle_MD+".input2Y",-0.5)
    cmds.setAttr(fan_angle_MD+".input2Z",-0.5)
    
    
    #axe_flip_node
    
    #X
    cmds.createNode("plusMinusAverage", name=fan_angle_X_PMA)
    cmds.connectAttr(fan_angle_MD+".outputX", fan_angle_X_PMA+".input1D[0]")

    #Y

    cmds.createNode("plusMinusAverage", name=fan_angle_Y_PMA)
    cmds.connectAttr(fan_angle_MD+".outputY", fan_angle_Y_PMA+".input1D[0]")
    #Z

    cmds.createNode("plusMinusAverage", name=fan_angle_Z_PMA)
    cmds.connectAttr(fan_angle_MD+".outputZ", fan_angle_Z_PMA+".input1D[0]")

    #connect_to_fan_roation
    cmds.connectAttr(fan_angle_X_PMA+".output1D", ft_fan_jnt+".rotateX")
    cmds.connectAttr(fan_angle_Y_PMA+".output1D", ft_fan_jnt+".rotateY") 
    cmds.connectAttr(fan_angle_Z_PMA+".output1D", ft_fan_jnt+".rotateZ")  
    
    #add attributes for fan control
    
    #menu_name
    cmds.addAttr(mid_jnt, longName="ft_fan_jnt",niceName="front fan joint" , attributeType="enum", enumName="-----")
    cmds.setAttr(mid_jnt+".ft_fan_jnt", keyable=True, lock=True)

    #flip_X
    cmds.addAttr(mid_jnt, longName="offset_orient_X_ft_fan_jnt", niceName="offset orient X", attributeType="float", min=-360, max=360, defaultValue=0)
    cmds.setAttr(mid_jnt+".offset_orient_X_ft_fan_jnt", keyable=True)
    cmds.connectAttr(mid_jnt+".offset_orient_X_ft_fan_jnt", fan_angle_X_PMA+".input1D[1]")

    #flip_Y
    cmds.addAttr(mid_jnt, longName="offset_orient_Y_ft_fan_jnt", niceName="offset orient Y", attributeType="float", min=-360, max=360, defaultValue=0)
    cmds.setAttr(mid_jnt+".offset_orient_Y_ft_fan_jnt", keyable=True)
    cmds.connectAttr(mid_jnt+".offset_orient_Y_ft_fan_jnt", fan_angle_Y_PMA+".input1D[1]")

    #flip_Z
    cmds.addAttr(mid_jnt, longName="offset_orient_Z_ft_fan_jnt", niceName="offset orient Z", attributeType="float", min=-360, max=360, defaultValue=0,keyable=True)
    cmds.connectAttr(mid_jnt+".offset_orient_Z_ft_fan_jnt", fan_angle_Z_PMA+".input1D[1]")
    
    #fan_jnt_scale

    #scale_x
    cmds.addAttr(mid_jnt, longName="scale_X_ft_fan_jnt", niceName="scale X" , attributeType="float", min=1, max=2, defaultValue=1, keyable=True)

    #scale_y
    cmds.addAttr(mid_jnt, longName="scale_Y_ft_fan_jnt", niceName="scale Y" , attributeType="float", min=1, max=2, defaultValue=1, keyable=True)

    #scale_z
    cmds.addAttr(mid_jnt, longName="scale_Z_ft_fan_jnt", niceName="scale Z" , attributeType="float", min=1, max=2, defaultValue=1, keyable=True)
    

    #parent_joint_max_rotation
    cmds.addAttr(mid_jnt, longName="max_rotation_ft_fan_jnt", niceName="max rotation" ,attributeType="long", min=0, max=360, defaultValue=90, keyable=True)

    #parent_joint_min_rotation
    cmds.addAttr(mid_jnt, longName="min_rotation_ft_fan_jnt", niceName="min rotation" ,attributeType="long", min=0, max=360, defaultValue=0, keyable=True)

    

    #create remapvalue name
    ft_fan_jnt_offset_scale_X_RV=ft_fan_jnt.replace("jnt","offset_scale_X_RV")
    ft_fan_jnt_offset_scale_Y_RV=ft_fan_jnt.replace("jnt","offset_scale_Y_RV")
    ft_fan_jnt_offset_scale_Z_RV=ft_fan_jnt.replace("jnt","offset_scale_Z_RV")
    
    #create remap value nodes
    cmds.createNode("remapValue",name=ft_fan_jnt_offset_scale_X_RV)
    cmds.setAttr(ft_fan_jnt_offset_scale_X_RV+".outputMin",1)
    
    cmds.createNode("remapValue",name=ft_fan_jnt_offset_scale_Y_RV)
    cmds.setAttr(ft_fan_jnt_offset_scale_Y_RV+".outputMin",1)
    
    cmds.createNode("remapValue",name=ft_fan_jnt_offset_scale_Z_RV)
    cmds.setAttr(ft_fan_jnt_offset_scale_Z_RV+".outputMin",1)

    
    #link_min_max_rotation
    cmds.connectAttr(mid_jnt+".max_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_X_RV+".inputMax")
    cmds.connectAttr(mid_jnt+".min_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_X_RV+".inputMin")

    cmds.connectAttr(mid_jnt+".max_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_Y_RV+".inputMax")
    cmds.connectAttr(mid_jnt+".min_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_Y_RV+".inputMin")

    cmds.connectAttr(mid_jnt+".max_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_Z_RV+".inputMax")
    cmds.connectAttr(mid_jnt+".min_rotation_ft_fan_jnt",ft_fan_jnt_offset_scale_Z_RV+".inputMin")

    #link_max_scale
    cmds.connectAttr(mid_jnt+".scale_X_ft_fan_jnt",ft_fan_jnt_offset_scale_X_RV+".outputMax")
    cmds.connectAttr(mid_jnt+".scale_Y_ft_fan_jnt",ft_fan_jnt_offset_scale_Y_RV+".outputMax")
    cmds.connectAttr(mid_jnt+".scale_Z_ft_fan_jnt",ft_fan_jnt_offset_scale_Z_RV+".outputMax")

    

    #fan axis choice
    
    #add_choice_attribute
    cmds.addAttr(mid_jnt, longName="choose_axis_X_ft_fan_jnt", niceName="choose X" , attributeType="byte", min=0, max=1, defaultValue=1, keyable=True)
    cmds.addAttr(mid_jnt, longName="choose_axis_Y_ft_fan_jnt", niceName="choose Y" , attributeType="byte", min=0, max=1, defaultValue=0, keyable=True)
    cmds.addAttr(mid_jnt, longName="choose_axis_Z_ft_fan_jnt", niceName="choose Z" , attributeType="byte", min=0, max=1, defaultValue=0, keyable=True)

    #condition_nodes_names
    ft_fan_jnt_axis_choice_X_C=ft_fan_jnt.replace("jnt","axis_choice_X_C")
    ft_fan_jnt_axis_choice_Y_C=ft_fan_jnt.replace("jnt","axis_choice_Y_C")
    ft_fan_jnt_axis_choice_Z_C=ft_fan_jnt.replace("jnt","axis_choice_Z_C")
    
    #condition_node_creation
    cmds.createNode("condition",name=ft_fan_jnt_axis_choice_X_C)
    cmds.createNode("condition",name=ft_fan_jnt_axis_choice_Y_C)
    cmds.createNode("condition",name=ft_fan_jnt_axis_choice_Z_C)
    
    #define_terms
    cmds.setAttr(ft_fan_jnt_axis_choice_X_C+".firstTerm",1)
    cmds.setAttr(ft_fan_jnt_axis_choice_Y_C+".firstTerm",1)
    cmds.setAttr(ft_fan_jnt_axis_choice_Z_C+".firstTerm",1)

    cmds.connectAttr(mid_jnt+".choose_axis_X_ft_fan_jnt", ft_fan_jnt_axis_choice_X_C+".secondTerm")
    cmds.connectAttr(mid_jnt+".choose_axis_Y_ft_fan_jnt", ft_fan_jnt_axis_choice_Y_C+".secondTerm")
    cmds.connectAttr(mid_jnt+".choose_axis_Z_ft_fan_jnt", ft_fan_jnt_axis_choice_Z_C+".secondTerm")
    
    #define_result
    cmds.setAttr(ft_fan_jnt_axis_choice_X_C+".colorIfFalseR",0)
    cmds.setAttr(ft_fan_jnt_axis_choice_Y_C+".colorIfFalseR",0)
    cmds.setAttr(ft_fan_jnt_axis_choice_Z_C+".colorIfFalseR",0)
    

    
    cmds.connectAttr(mid_jnt+".rotateX",ft_fan_jnt_axis_choice_X_C+".colorIfTrueR")
    cmds.connectAttr(mid_jnt+".rotateY",ft_fan_jnt_axis_choice_Y_C+".colorIfTrueR")
    cmds.connectAttr(mid_jnt+".rotateZ",ft_fan_jnt_axis_choice_Z_C+".colorIfTrueR")
    
    #PMA merge name
    
    rotation_value_merged=ft_fan_jnt.replace("jnt","rotaion_value_combined")
    
    cmds.createNode("plusMinusAverage", name=rotation_value_merged)
    
    #link conditions
    
    cmds.connectAttr(ft_fan_jnt_axis_choice_X_C+".outColor.outColorR",rotation_value_merged+".input1D[0]")
    cmds.connectAttr(ft_fan_jnt_axis_choice_Y_C+".outColor.outColorR",rotation_value_merged+".input1D[1]")
    cmds.connectAttr(ft_fan_jnt_axis_choice_Z_C+".outColor.outColorR",rotation_value_merged+".input1D[2]")
    
    cmds.connectAttr(rotation_value_merged+".output1D",ft_fan_jnt_offset_scale_X_RV+".inputValue")
    cmds.connectAttr(rotation_value_merged+".output1D",ft_fan_jnt_offset_scale_Y_RV+".inputValue")
    cmds.connectAttr(rotation_value_merged+".output1D",ft_fan_jnt_offset_scale_Z_RV+".inputValue")
    
    cmds.connectAttr(ft_fan_jnt_offset_scale_X_RV+".outValue",ft_fan_jnt+".scaleX")
    cmds.connectAttr(ft_fan_jnt_offset_scale_Y_RV+".outValue",ft_fan_jnt+".scaleY")
    cmds.connectAttr(ft_fan_jnt_offset_scale_Z_RV+".outValue",ft_fan_jnt+".scaleZ")
    
