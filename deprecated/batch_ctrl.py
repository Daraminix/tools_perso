import maya.cmds as cmds

def bulkLollipop(jntSelection=[]):
	
	for jnt in jntSelection:

		ctrlName = jnt.replace('_skn_jnt', '_ctrl')
		groupName = jnt.replace('_skn_jnt', '_ctrl_offset_grp')
		circleName = jnt.replace('_skn_jnt', '_circleShape')
		lineName = jnt.replace('_skn_jnt', '_lineShape')
		snapperName = jnt.replace('_skn_jnt', '_ctrl_snap_grp')




		
		circleCrl = cmds.circle(centerZ=3, normal=(0,1,0),name=circleName)
		lineCtrl  = cmds.curve(p=[(0,0,2),(0,0,0)], d=1, name=lineName)

		shapeNode = cmds.listRelatives(lineCtrl, shapes=True)
		ctrl = cmds.parent(shapeNode, circleCrl[0], add=True, s=True)
		cmds.delete(lineCtrl)

		# ctrl = cmds.rename(ctrl, ctrlName)

		group = cmds.group(em=True, w=True, n=groupName)
		snapper = cmds.group(em=True, w=True, n=snapperName)

		cmds.parent(ctrl, snapper)
		cmds.parent(snapper, group)

		cmds.matchTransform(group,jnt)
		ctrl = cmds.listRelatives(snapper, shapes=False, type='transform', children=True) # (ctrl[0].split('|')[0]
		ctrl = cmds.rename(ctrl[0], ctrlName)
		cmds.parentConstraint(ctrl, jnt, maintainOffset=False)
