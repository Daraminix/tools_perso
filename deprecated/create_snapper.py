import maya.cmds as cmds

def CreateSnapper():
    #create_snap_grp

    jnts = cmds.ls(sl=True)

    for jnt in jnts:
        snap_name=jnt.replace("jnt","snapper")
        cmds.group(em=True,world=True,name=snap_name)
        cmds.parentConstraint(jnt,snap_name,mo=0)
        